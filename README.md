# Serverless Email Resender
Uses AWS SES and Lambda to resend all emails from one or more domains to a single mailbox.

## Usage
Create an S3 bucket to store your emails in. (Optionally, it might be a good idea to set up a lifecycle policy to make the emails expire).

Paste the contents of `handler.py` into a lambda function and configure the appropriate environment variables (see the code).

The function's execution role must have read and write access to your S3 bucket as well as permission to send Emails on SES.

Create a Rule Set in SES which has rules for each of the domains / addresses you want, and does two things:

 1. Writes the email to your S3 bucket
 2. Calls the Lambda function as Event

## Notes
 - You must set the `RESENDER_EMAIL` to be an address you are able to send from using SES (i.e. it's verified)
