"""
SES email forwarder

Forwards every email it gets onward to a given address. Great for managing lots of domains.
@author Mitchell Grice https://gricey.net
Inspired by https://github.com/arithmetric/aws-lambda-ses-forwarder
"""
from __future__ import print_function
import os
import json
import re
import ast
import email

import boto3


EMAIL_BUCKET_NAME = os.environ["EMAIL_BUCKET_NAME"]
RESENDER_EMAIL = os.environ["RESENDER_EMAIL"]
TARGET_EMAIL = os.environ["TARGET_EMAIL"]
DEBUG_LOG_EVENT = ast.literal_eval(os.environ.get("DEBUG_LOG_EVENT", "False"))


s3 = boto3.client('s3')
ses = boto3.client('ses')


def lambda_handler(event, context):
    # Debug
    if DEBUG_LOG_EVENT:
        print(json.dumps(event))

    # Pull email data from event
    email_data = event["Records"][0]["ses"]["mail"]
    message_id = email_data["messageId"]
    print("Processing email ID: {}".format(message_id))

    # Load email from S3
    # Need to copy the object first to ensure read access
    s3.copy_object(
        Bucket=EMAIL_BUCKET_NAME,
        CopySource="{}/{}".format(EMAIL_BUCKET_NAME, message_id),
        Key=message_id,
        ACL='private',
        ContentType='text/plain',
        StorageClass='STANDARD',
    )
    response = s3.get_object(
        Bucket=EMAIL_BUCKET_NAME,
        Key=message_id,
    )
    raw_email = str(response['Body'].read())

    # Process email
    em = email.message_from_string(raw_email)  # type: email.message.Message

    # Add the "Reply-To" header with the original sender's address if not already present
    if "Reply-To" not in em:
        em.add_header("Reply-To", em.get("From"))

    # Replace "From" header
    em.replace_header("From", "{} <{}>".format(
            re.sub(r"<(.*)>", "", em.get("From")).strip(),
            RESENDER_EMAIL,
        )
    )

    # Remove "Return-Path", "Sender", "DKIM-Signature" headers
    del em["Return-Path"]
    del em["Sender"]
    del em["DKIM-Signature"]

    # Glue back together to make a new email
    new_raw_email = em.as_string()

    # Send new email
    ses.send_raw_email(
        Destinations=[TARGET_EMAIL],
        RawMessage={
            "Data": new_raw_email,
        }
    )
